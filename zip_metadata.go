package main

import (
	"archive/zip"
	"bytes"
	"compress/gzip"
	"encoding/binary"
	"encoding/json"
	"flag"
	"log"
	"os"
	"strconv"
)

type Metadata struct {
	Modified int64  `json:"modified"`
	CRC      uint32 `json:"crc,omitempty"`
	Size     uint64 `json:"size,omitempty"`
	Zipped   uint64 `json:"zipped,omitempty"`
	Mode     string `json:"mode,omitempty"`
	Comment  string `json:"comment,omitempty"`
}

func main() {
	flag.Parse()
	args := flag.Args()

	file, output := args[0], args[1]
	zipWriteMetadata(file, output)
}

func zipWriteMetadata(file, output string) {

	archive, err := zip.OpenReader(file)
	if err != nil {
		log.Fatal(err)
	}
	defer archive.Close()

	f, _ := os.Create(output)
	gz := gzip.NewWriter(f)
	defer f.Close()
	defer gz.Close()

	gz.Write(stringEntry("GitLab Build Artifacts Metadata 0.0.2\n"))
	gz.Write(stringEntry("{}")) // no errors in this version supported

	for _, entry := range archive.File {
		line := pathData(entry)
		gz.Write(line)
	}

}

func pathData(file *zip.File) []byte {
	var buffer bytes.Buffer

	buffer.Write(stringEntry(file.Name))

	meta := newMetadata(file)
	m, _ := json.Marshal(meta)
	m = append(m, byte('\n'))

	buffer.Write(stringEntry(string(m)))

	return buffer.Bytes()
}

func stringEntry(str string) []byte {
	var buffer bytes.Buffer

	binary.Write(&buffer, binary.BigEndian, uint32(len(str)))
	buffer.WriteString(str)

	return buffer.Bytes()
}

func newMetadata(file *zip.File) Metadata {
	return Metadata{
		Modified: file.ModTime().Unix(),
		CRC:      file.CRC32,
		Size:     file.UncompressedSize64,
		Zipped:   file.CompressedSize64,
		Mode:     strconv.FormatUint(uint64(file.Mode().Perm()), 8),
		Comment:  file.Comment}
}
